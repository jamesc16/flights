import collections
import datetime
from operator import itemgetter
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from yattag import Doc, indent
from scipy.stats import multivariate_normal

privacy_year = 2017

airports_file = np.genfromtxt('airports.txt', delimiter=',', dtype='str')

textc = '#323232'  # color for text in graphs
backc = '#ececec'  # color for background in graphs
headc = '#65799b'  # color for drawings in graphs

engine_manufacts_abbr = dict(PW='Pratt & Whitney', GE='General Electric', RR='Rolls Royce', CFMI='CFM International',
                             IAE='International Aero Engines', PWC='Pratt & Whitney Canada', LY='Lycoming',
                             EA='Engine Alliance', GAR='Garrett AiResearch')

cities = {'LON': ['LHR', 'LGW', 'LCY', 'LTN', 'STN', 'SEN', 'BQH'],
          'MOW': ['SVO', 'DME', 'VKO'],
          'MIL': ['MXP', 'LIN'],
          'PAR': ['CDG', 'ORY', 'LBG'],
          'ROM': ['FCO', 'CIA'],
          'STO': ['ARN', 'BMA', 'NYO'],
          'CHI': ['ORD', 'MDW'],
          'QDF': ['DFW', 'DAL'],
          'QHO': ['IAH', 'HOU'],
          'QLA': ['LAX', 'ONT', 'SNA', 'BUR', 'LGB'],
          'QMI': ['MIA', 'FLL', 'PBI'],
          'NYC': ['JFK', 'LGA', 'EWR'],
          'QSF': ['SFO', 'SJC', 'OAK'],
          'WAS': ['IAD', 'DCA', 'BWI']}


def gc_distance(airport1, airport2):
    """Calculate the distance in km between two airports.

    Args:
        airport1 (Airport): the first airport
        airport2 (Airport): the second airport

    Returns:
        float: the distance in km between the two airports
    """
    p1 = airport1.lat * 2.0 * np.pi/360
    p2 = airport2.lat * 2.0 * np.pi/360
    l1 = airport1.lon * 2.0 * np.pi/360
    l2 = airport2.lon * 2.0 * np.pi/360
    r = 6371.01
    gcd = 2 * r * np.arcsin(np.sqrt(np.sin((p2-p1)/2)**2 + np.cos(p1) * np.cos(p2) * np.sin((l2-l1)/2)**2))
    return gcd


def write_airport(a, airport_type='normal'):
    """Generate the HTML representation of an airport.

    Args:
        a (Airport): the airport
        airport_type (str): 'normal', 'scheduled' (but never reached), or 'diverted'

    Returns:
        string: HTML code for the airport with abbreviation
    """
    if a.country == 'United States':
        s = '<abbr title="' + a.name + ', ' + a.city + ', ' + a.state
    else:
        s = '<abbr title="' + a.name + ', ' + a.city + ', ' + a.country
    if airport_type == 'scheduled':
        s += ' (scheduled)"><span style="color:gray;font-style:italic;">'
    elif airport_type == 'diverted':
        s += ' (diverted)"><span style="color:red;">'
    else:
        s += '">'
    if a.iata == '' and a.icao == '':
        s += a.name[:3] + '&#8230;'
    else:
        s += a.iata
    if airport_type == 'scheduled' or airport_type == 'diverted':
        s += '</span>'
    s += '</abbr>'
    return s


def write_city(city, airports_tally):
    airports = cities_inv[city]
    abbr = ''
    for airport in airports:
        count = [row[1] for row in airports_tally if row[0] == airport]
        try:
            abbr += airport + ' (' + str(count[0]) + '), '
        except IndexError:
            abbr += airport + ' (0), '
    abbr = abbr[:-2]  # remove final comma and space
    text = '<abbr title="' + abbr + '">' + city + '</abbr>'
    return text


def write_segment(segment):
    """Generate the HTML representation of a segment.

    Args:
        segment (list): a list of two airport objects

    Returns:
        string: HTML code for the segment with arrow and abbreviations
    """
    s = write_airport(segment[0]) + ' &rarr; ' + write_airport(segment[1])
    return s


# TODO: there is a proper way to do this
def get_airports(r):
    """Get the airports from a list of segments.

    Args:
        r (list): a list of segments eg [['ABC', 'DEF'], ['DEF', 'xyz']]

    Returns:
        list: a list of the airport codes that appear in the segments
    """
    a = [r[0][0], r[0][1]]
    for i in range(1, len(r)):
        if r[i][0] != a[-1]:
            a.append(r[i][0])
        a.append(r[i][1])
    return a


def collect_counts(s):
    """Tally the number of times each item appears in a list.

    Args:
        s (list): a list of strings or numbers

    Returns:
        list: a list of lists [item, how many times it appeared] sorted by appearances descending
    """
    s_counts = []
    for a in s:
        if not(a in [x[0] for x in s_counts]):
            s_counts.append([a, s.count(a)])
    s_counts = sorted(s_counts, key=itemgetter(0))
    s_counts = sorted(s_counts, reverse=True, key=itemgetter(1))
    return s_counts


def create_date(s):
    """Create a datetime object, possibly with unspecified month or day.

    Args:
        s (str): date specified by yyyymmdd, yyyymm, or yyyy, eg 19990115

    Returns:
        date: the specified date assuming month is June and day is 15 if they are not provided
    """
    if len(s) == 8:
        date = datetime.date(int(s[:4]), int(s[4:6]), int(s[6:]))
    elif len(s) == 6:
        date = datetime.date(int(s[:4]), int(s[4:]), 15)
    elif len(s) == 4:
        date = datetime.date(int(s[:4]), 6, 15)
    else:
        return None
    return date


def print_set(s):
    """Sort a set alphabetically and generate its HTML representation with one element on each line.

    Args:
        s (set): a set of things to list in a column

    Returns:
        str: HTML for all elements in a column sorted alphabetically
    """
    s = list(s)
    s = sorted(s)
    output = str(s[0])
    for x in s[1:]:
        output += '<br>' + str(x)
    return output


def gc_distance_route(route):
    """Calculate the great circle distance for a route.

    Args:
        route (list): a list of segments

    Returns:
        float: the great circle distance of the route
    """
    d = 0.0
    for segment in route:
        d += gc_distance(Airport(segment[0]), Airport(segment[1]))
    return d


def parse_route(rstring):
    """Generate a list of segments and an HTML route display given a route with diversions and scheduled stops.

    Args:
        rstring (str): a hyphenated sequence of airport codes, with a preceding 's' indicating that the stop was
                       scheduled but never landed and a preceding 'd' indicating that the stop was a diversion.
                       for example, 'ABC-sDEF-dxyz' indicates a scheduled flight from ABC to DEF which never landed at
                       DEF but diverted to xyz.

    Returns:
        list: a list of lists for each segment [['ABC','DEF'], ['DEF', 'GHI']]
        str: HTML code for the route with arrows, abbreviations, and line breaks

    """
    all_nodes = rstring.split('-')
    route = [all_nodes[0]]
    display_rstring = write_airport(Airport(all_nodes[0]))
    pos = 1
    for node in all_nodes[1:]:
        if node[0] == 's' and len(node) == 4:
            if pos < 2:
                display_rstring += ' &#8628; ' + write_airport(Airport(node[1:]), airport_type='scheduled')
            else:
                display_rstring += '<br><span style="visibility:hidden;">' + all_nodes[0] + '</span> &#8628; ' +\
                                   write_airport(Airport(node[1:]), airport_type='scheduled')
        elif node[0] == 'd' and len(node) == 4:
            if pos < 2:
                route.append(node[1:])
                display_rstring += ' &rarr; ' + write_airport(Airport(node[1:]), airport_type='diverted')
            else:
                route.append(node[1:])
                display_rstring += '<br><span style="visibility:hidden;">' + all_nodes[0] + '</span> &rarr; ' +\
                                   write_airport(Airport(node[1:]), airport_type='diverted')
        else:
            if pos < 2:
                route.append(node)
                display_rstring += ' &rarr; ' + write_airport(Airport(node))
            else:
                route.append(node)
                display_rstring += '<br><span style="visibility:hidden;">' + all_nodes[0] + '</span> &rarr; ' +\
                                   write_airport(Airport(node))
        pos += 1
    for i in range(len(route)-1):
        route[i] = [route[i], route[i+1]]
    del(route[-1])
    return route, display_rstring


def heat_map(projection, sigma=1.5, n=1.4):
    """Compute a mixture of Gaussians centered on each airport and display the mixture as a heat map.

    Args:
        projection (str): map projection to use
        sigma (float): values on the diagonal for the covariance matrix
        n (float): smoothing factor

    Returns:
        nothing. the map is saved as 'density.png' in the local directory

    """
    plt.close('all')
    fig = plt.figure(figsize=(14, 7))

    x, y = np.mgrid[-180:180:.5, -90:90:.5]
    world = np.empty(x.shape + (2,))
    world[:, :, 0] = x
    world[:, :, 1] = y

    test_spike = multivariate_normal([0, 0], [[1, 0], [0, 1]])
    mixture = np.empty(test_spike.pdf(world).shape)

    for airport in airports_objects:
        # print(airport.code)
        mean = [airport.lon, airport.lat]
        covariance = sigma * np.array([[1.0, 0.0], [0.0, 1.0]])
        spike = multivariate_normal(mean, covariance)
        mixture += spike.pdf(world)

    # Apply an operation which tends to equalize the mixture
    mixture = ((mixture + 1) ** (1 - n) - 1) / (1 - n)

    # cs = m.contourf(x[:,0], y[0,:], mixture, cmap=plt.get_cmap('Blues'), latlon=True, zorder=100, alpha=0.4)
    # plt.show()

    ax = fig.gca()
    fig.subplots_adjust(left=0.01)
    fig.subplots_adjust(right=0.99)
    fig.subplots_adjust(top=0.99)
    fig.subplots_adjust(bottom=0.01)
    # plt.contourf(x, y, mixture, cmap=plt.get_cmap('Blues'))
    ax.pcolor(x, y, mixture, cmap=plt.get_cmap('Blues'))
    plt.xlim(-180, 180)
    plt.ylim(-90, 90)
    plt.xticks([])
    plt.yticks([])
    plt.savefig('density.png', pad_inches=0)


def make_map(projection):
    """Draw a map of all flight geodesics using basemap.

    Args:
        projection (str): 'mill' for Miller cylindrical, 'aeqd' for azimuthal equidistant,
        'na' for stereographic centered on North America, 'eu' for stereographic centered on Europe

    Returns:
        Nothing. The map is saved as 'map_<projection>.png' in the local directory.
    """

    fig = plt.figure()

    if projection == 'mill':
        gap = 0.05
        m = Basemap(projection='mill', lon_0=1, resolution='l')

    elif projection == 'aeqd':
        lon_0 = -97.0
        lat_0 = 32.0
        gap = 0.0001
        m = Basemap(projection='aeqd', lat_0=lat_0, lon_0=lon_0, resolution='l')

    elif projection == 'na':
        gap = 0
        m = Basemap(width=9700000, height=7050000, resolution='l', projection='stere', lat_ts=50, lat_0=45, lon_0=-115.)

    elif projection == 'eu':
        gap = 0
        m = Basemap(width=7840000, height=5700000, resolution='i', projection='stere', lat_ts=57, lat_0=57, lon_0=0.)

    else:
        gap = 0.05
        m = Basemap(projection='mill', lon_0=1, resolution='l')

    m.shadedrelief(scale=0.45)

    for airport in airports_norepeat_obj:
        m.scatter(airport.lon, airport.lat, s=3.5, latlon=True, color='black', zorder=100)
        m.scatter(airport.lon, airport.lat, s=0.7, latlon=True, color='white', zorder=200)

    for segment in segments_object:
        c = 'black'

        if projection == 'na':
            if segment[0].continent != 'North America' or segment[1].continent != 'North America':
                c = '#816f91'
                zo = 10
        if projection == 'eu':
            if segment[0].continent != 'Europe' or segment[1].continent != 'Europe':
                c = '#816f91'
                zo = 10
        else:
            zo = 50

        lon0 = segment[0].lon
        lat0 = segment[0].lat
        lon1 = segment[1].lon
        lat1 = segment[1].lat

        # TODO: zorder may not be working here
        if abs(lon0 - lon1) < 180:
            m.drawgreatcircle(lon0, lat0, lon1, lat1, linewidth=0.5, color=c, zorder=zo)

        else:
            lon0 *= np.pi / 180
            lat0 *= np.pi / 180
            lon1 *= np.pi / 180
            lat1 *= np.pi / 180
            if lon0 < 0:
                lons0 = np.linspace(lon1, np.pi)
                lons1 = np.linspace(-np.pi + gap, lon0)
            else:
                lons0 = np.linspace(lon0, np.pi)
                lons1 = np.linspace(-np.pi + gap, lon1)

            lats0 = np.arctan((
                                  np.sin(lat0) * np.cos(lat1) * np.sin(lons0 - lon1) - np.sin(lat1) * np.cos(
                                      lat0) * np.sin(
                                      lons0 - lon0)) / (np.cos(lat0) * np.cos(lat1) * np.sin(lon0 - lon1)))

            lats1 = np.arctan((
                                  np.sin(lat0) * np.cos(lat1) * np.sin(lons1 - lon1) - np.sin(lat1) * np.cos(
                                      lat0) * np.sin(
                                      lons1 - lon0)) / (np.cos(lat0) * np.cos(lat1) * np.sin(lon0 - lon1)))

            lat0, lon0, lat1, lon1, lats0, lons0, lats1, lons1 = (x * 180 / np.pi for x in
                                                                  (lat0, lon0, lat1, lon1, lats0,
                                                                   lons0, lats1, lons1))

            m.plot(lons0, lats0, linewidth=0.5, color=c, latlon=True, zorder=zo)
            m.plot(lons1, lats1, linewidth=0.5, color=c, latlon=True, zorder=zo)

    fig.subplots_adjust(left=0.01)
    fig.subplots_adjust(bottom=0)
    fig.subplots_adjust(right=.99)
    fig.subplots_adjust(top=1.)
    fig.subplots_adjust(wspace=0)
    fig.subplots_adjust(hspace=0)

    filename = 'map_' + projection + '.png'
    plt.savefig(filename, dpi=180)


def distance_line():
    """Draw a line graph showing distance flown in each year.
    """
    distances = [[flight.date.year, flight.dist] for flight in flights_objects]
    years = []
    dists = []
    for year in range(min(d[0] for d in distances), max(d[0] for d in distances) + 1):
        years.append(year)
        dists.append(sum(d[1] for d in distances if d[0] == year))

    fig = plt.figure(figsize=(6, 3))
    plt.xlim(min(years), max(years))
    plt.ylim(0, max(dists)*1.1)
    plt.xlabel('year')
    plt.ylabel('distance flown (km)')

    plt.tick_params(axis='both', which='major', labelsize=7.5)
    matplotlib.rcParams.update({'font.size': 7.5})
    fig.subplots_adjust(left=0.12)
    fig.subplots_adjust(bottom=.14)
    fig.subplots_adjust(right=.97)
    fig.subplots_adjust(top=.97)
    fig.subplots_adjust(wspace=.2)
    fig.subplots_adjust(hspace=.2)

    ax = plt.gca()
    ax.patch.set_facecolor(backc)
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_color(textc)
    ax.spines['bottom'].set_color(textc)
    ax.xaxis.label.set_color(textc)
    ax.yaxis.label.set_color(textc)
    ax.tick_params(axis='x', colors=textc)
    ax.tick_params(axis='y', colors=textc)

    plt.plot(years, dists, color=headc, linewidth=1.5)
    plt.savefig('distance_line.png')


def distance_gram(distances):
    """Draw a histogram of segment distances.
    """
    fig = plt.figure(figsize=(6, 3))

    distances = [d[1] for d in distances]
    plt.hist(distances, bins=np.arange(0, max(distances) + 500, 500), normed=0, facecolor=headc, edgecolor=textc)

    plt.xlabel('segment distance (km)')
    plt.ylabel('count')
    fig.subplots_adjust(left=0.12)
    fig.subplots_adjust(bottom=.14)
    fig.subplots_adjust(right=.97)
    fig.subplots_adjust(top=.97)
    fig.subplots_adjust(wspace=.2)
    fig.subplots_adjust(hspace=.2)

    ax = plt.gca()
    ax.patch.set_facecolor(backc)

    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_color(textc)
    ax.spines['bottom'].set_color(textc)
    ax.xaxis.label.set_color(textc)
    ax.yaxis.label.set_color(textc)
    ax.tick_params(axis='x', colors=textc)
    ax.tick_params(axis='y', colors=textc)

    plt.savefig('distance_gram.png')


def age_gram(ages):
    """Draw a histogram of airplane ages.
    """
    fig = plt.figure(figsize=(6, 3))

    ages = [age[1] for age in ages]
    plt.hist(ages, bins=np.arange(0, max(ages) + 1, 1), normed=0, facecolor=headc, edgecolor=textc)

    plt.xlim(0, int(max(ages))+1)
    plt.xlabel('airplane age (years)')
    plt.ylabel('count')
    fig.subplots_adjust(left=0.12)
    fig.subplots_adjust(bottom=0.14)
    fig.subplots_adjust(right=0.97)
    fig.subplots_adjust(top=0.97)
    fig.subplots_adjust(wspace=0.2)
    fig.subplots_adjust(hspace=0.2)

    ax = plt.gca()
    ax.patch.set_facecolor(backc)

    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_color(textc)
    ax.spines['bottom'].set_color(textc)
    ax.xaxis.label.set_color(textc)
    ax.yaxis.label.set_color(textc)
    ax.tick_params(axis='x', colors=textc)
    ax.tick_params(axis='y', colors=textc)

    plt.savefig('age_gram.png')


def time_gram():
    dep_times = []
    for flight in flights_objects:
        if flight.std is not None:
            dep_times.append(int(flight.std[:-1]))

    fig = plt.figure(figsize=(6, 3))

    plt.hist(dep_times, bins=np.arange(0, 2400, 100), normed=0, facecolor=headc, edgecolor=textc)

    plt.xlim(0, 2400)
    plt.xlabel('local departure time')
    plt.ylabel('count')
    fig.subplots_adjust(left=0.12)
    fig.subplots_adjust(bottom=0.14)
    fig.subplots_adjust(right=0.97)
    fig.subplots_adjust(top=0.97)
    fig.subplots_adjust(wspace=0.2)
    fig.subplots_adjust(hspace=0.2)

    ax = plt.gca()
    ax.patch.set_facecolor(backc)

    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_color(textc)
    ax.spines['bottom'].set_color(textc)
    ax.xaxis.label.set_color(textc)
    ax.yaxis.label.set_color(textc)
    ax.tick_params(axis='x', colors=textc)
    ax.tick_params(axis='y', colors=textc)

    plt.savefig('time_gram.png')


def delay_gram():
    delay_times = []
    for flight in flights_objects:
        if flight.std is not None and flight.atd is not None:
            std = datetime.time(int(flight.std[:2]), int(flight.std[2:4]))
            atd = datetime.time(int(flight.atd[:2]), int(flight.atd[2:4]))
            #TODO: subtract properly
            hours_diff = atd.hour - std.hour
            mins_diff = atd.minute - std.minute
            time_diff = 60*hours_diff + mins_diff
            delay_times.append(time_diff)

    fig = plt.figure(figsize=(6, 3))

    plt.hist(delay_times, bins=np.arange(-20, 200, 5), normed=0, facecolor=headc, edgecolor=textc)

    #plt.xlim(0, 2400)
    plt.xlabel('departure delay (min)')
    plt.ylabel('count')
    fig.subplots_adjust(left=0.12)
    fig.subplots_adjust(bottom=0.14)
    fig.subplots_adjust(right=0.97)
    fig.subplots_adjust(top=0.97)
    fig.subplots_adjust(wspace=0.2)
    fig.subplots_adjust(hspace=0.2)

    ax = plt.gca()
    ax.patch.set_facecolor(backc)

    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_color(textc)
    ax.spines['bottom'].set_color(textc)
    ax.xaxis.label.set_color(textc)
    ax.yaxis.label.set_color(textc)
    ax.tick_params(axis='x', colors=textc)
    ax.tick_params(axis='y', colors=textc)

    plt.savefig('delay_gram.png')


# Not in use. Engines is in a table.
def engine_bar(engines):
    fig = plt.figure(figsize=(6, 3))
    engines = [engine[:engine.index(' ')] for engine in engines]
    engines_tally = collect_counts(engines)
    #print(engines_tally)
    bars = plt.bar(np.arange(len(engines_tally)), [x[1] for x in engines_tally], 0.7, color=headc, linewidth=1.5)
    ax = fig.gca()

    for bar, man in zip(bars,[x[0] for x in engines_tally]):
        h = bar.get_height()
        if h < 10:
            h += 4
            pos = 'bottom'
            label_color = textc
        elif h >= 10:
            h -= 4
            pos = 'top'
            label_color = '#ffffff'
        ax.text(bar.get_x() + bar.get_width()/2, h, man, rotation=90, ha='center', va=pos, color=label_color)
    #plt.show()
    return engines_tally


class Airport:

    def __init__(self, code):
        self.code = code
        self.iata = None
        self.icao = None
        for i in range(len(airports_file)):
            if airports_file[i,0] == code:
                self.iata = airports_file[i,1]
                self.icao = airports_file[i,2]
                self.lat = float(airports_file[i,3])
                self.lon = float(airports_file[i,4])
                self.elevation = int(airports_file[i,5])
                self.name = airports_file[i,6]
                self.city = airports_file[i,7]
                self.state = airports_file[i,8]
                self.country = airports_file[i,9]
                self.continent = airports_file[i,10]
                self.opened = create_date(airports_file[i,11])
                self.closed = create_date(airports_file[i,12])
                break


class Flight:

    def __init__(self, route, mkt_cxr=None, adm_cxr=None, ope_cxr=None, desig=None, number=None,
                 date=None, seat=None, seat_type=None, std=None, sta=None, atd=None, ata=None, registration=None,
                 manufacturer=None, type1=None, type2=None, type3=None, pics=None, cabin=None, ln=None, msn=None,
                 num_engines=None, engines=None, first_flight=None, actual_dist=None, runways=None, gates=None,
                 comment = None):
        self.mkt_cxr = mkt_cxr
        self.adm_cxr = adm_cxr
        self.ope_cxr = ope_cxr

        self.desig = desig
        self.number = number

        if runways is not None:
            self.runways = runways.split(';')
        else:
            self.runways = None

        if gates is not None:
            self.gates = gates.split(';')
        else:
            self.gates = None

        if date is not None:
            self.date = create_date(str(date))
        else:
            self.date = None

        self.comment = comment

        self.std = std
        self.sta = sta
        self.atd = atd
        self.ata = ata
        self.actual_dist = actual_dist
        self.route = route
        route_list, route_display = parse_route(route)

        self.route_list = route_list
        self.route_display = route_display
        self.seat = seat
        self.seat_type = seat_type
        self.cabin = cabin

        self.num_engines = num_engines
        self.engines = engines

        if first_flight is not None:
            self.first_flight = create_date(str(first_flight))
            self.first_flight_known = first_flight
        else:
            self.first_flight = None

        self.registration = registration
        self.manufacturer = manufacturer
        self.type1 = type1
        if manufacturer is not None and type2 is not None:
            self.type2 = manufacturer + ' ' + type2
        else:
            self.type2 = None
        self.type3 = type3

        self.ln = ln
        self.msn = msn
        self.dist = gc_distance_route(route_list)

        if pics is not None:
            pics = pics.split(';')
        self.pics = pics


flights_objects = []

with open('flights.txt', mode='r') as document:
    for line in document:
        flight = {}
        line = line.split(',')
        for item in line:
            item = item.split('=')
            if hasattr(Flight('DFW-DFW'), item[0]):
                # requires that all arguments of init are attributes.??
                flight[item[0]] = item[1].strip()
        w = Flight(**flight)
        flights_objects.append(w)

airplanes = [flight.type2 for flight in flights_objects]
airplanes_tally = collect_counts(airplanes)

airplane_dates = [[flight.type2, flight.date] for flight in flights_objects]
# y =[x for x in airplane_dates if x[0] == 'Boeing 737-800']
airplane_distances = [[flight.type2, flight.dist, flight.route_display] for flight in flights_objects]

airlines = [flight.mkt_cxr for flight in flights_objects]
airlines_tally = collect_counts(airlines)

operators = [flight.adm_cxr for flight in flights_objects]
operated_by = []
for i in range(len(airlines)):
    if airlines[i] != operators[i] and operators[i] is not None:
        if not(airlines[i] in [x[0] for x in operated_by]):
            operated_by.append([airlines[i], [operators[i], 1]])
        else:
            for j in range(len(operated_by)):
                if operated_by[j][0] == airlines[i]:
                    if not(operators[i] in [x[0] for x in operated_by[j]]):
                        operated_by[j].append([operators[i], 1])
                    else:
                        for k in range(len(operated_by[j])):
                            if operated_by[j][k][0] == operators[i]:
                                operated_by[j][k][1] += 1
                                break


for j in range(len(operated_by)):
    operated_by[j] = sorted(operated_by[j], reverse=True, key=itemgetter(1))

manufacts = [flight.manufacturer for flight in flights_objects]
manufacts_tally = collect_counts(manufacts)

airports_per_flight = [get_airports(flight.route_list) for flight in flights_objects]
airports = []
for i in range(len(airports_per_flight)):
    for j in range(len(airports_per_flight[i])):
        airports.append(airports_per_flight[i][j])
airports_tally = collect_counts(airports)

airports_objects = [Airport(x) for x in airports]

states = [x.state for x in airports_objects if x.country == 'United States']
states_tally = collect_counts(states)

countries = [x.country for x in airports_objects]
countries_tally = collect_counts(countries)

continents = [x.continent for x in airports_objects]
continents_tally = collect_counts(continents)


num_flights = len(flights_objects)
num_airports = len(airports_tally)
num_types = len(airplanes_tally)

total_distance = 0
num_segments = 0
for flight in flights_objects:
    total_distance += gc_distance_route(flight.route_list)
    num_segments += len(flight.route_list)

mean_distance = total_distance / num_segments
circums = total_distance / 40075.0

segments = []
for flight in flights_objects:
    for segment in flight.route_list:
        segments.append(segment)

segments_object = [[Airport(x[0]), Airport(x[1])] for x in segments]
segment_distances = [[segment, gc_distance(segment[0], segment[1])] for segment in segments_object]



segment_counts = collect_counts(segments)

land_routes = []
for i in range(len(flights_objects) - 1):
    if flights_objects[i].route_list[0][0] != flights_objects[i+1].route_list[-1][-1]:
        land_route = [Airport(flights_objects[i+1].route_list[-1][-1]), Airport(flights_objects[i].route_list[0][0])]
        land_distance = gc_distance(land_route[0], land_route[1])
        x, land_route_display = parse_route(land_route[0].code + '-' + land_route[1].code)
        if not(land_routes.__contains__([land_route_display, land_distance])):
            land_routes.append([land_route_display, land_distance])

segment_distances = sorted(segment_distances, key=itemgetter(0))
segment_distances = sorted(segment_distances, reverse=True, key=itemgetter(1))

segment_distances_norepeat = []
for s in segment_distances:
    same = False
    for s2 in segment_distances_norepeat:
        if s[0][0].code == s2[0][0].code and s[0][1].code == s2[0][1].code:
            same = True
    if not same:
        segment_distances_norepeat.append(s)


land_routes = sorted(land_routes, key=itemgetter(0))
land_routes = sorted(land_routes, reverse=True, key=itemgetter(1))

#print(land_routes)

longest_segment_d = 0.0
shortest_segment_d = 1.0e10
longest_segment = []
shortest_segment = []
for segment_info in segment_distances:
    if 0.1 < segment_info[1] < shortest_segment_d:
        shortest_segment_d = segment_info[1]
        shortest_segment = segment_info[0]
    if segment_info[1] > longest_segment_d:
        longest_segment_d = segment_info[1]
        longest_segment = segment_info[0]

airports_norepeat = [x[0] for x in airports_tally]
airports_norepeat_obj = [Airport(x) for x in airports_norepeat]

northernmost = -90.0
southernmost = 90.0
northernmost_airport = ''
southernmost_airport = ''
for airport in airports_norepeat_obj:
    lat = airport.lat
    if lat > northernmost:
        northernmost = lat
        northernmost_airport = airport
    if lat < southernmost:
        southernmost = lat
        southernmost_airport = airport

south_degrees = ''
if southernmost < 0:
    south_degrees += '&#x2212;'
south_degrees += str(round(abs(southernmost), 2)) + '&#176;'


highest = -1000000.0
lowest = 1000000.0
highest_airport = 'init'
lowest_airport = 'init'
for airport in airports_norepeat_obj:
    h = airport.elevation
    if h > highest:
        highest = h
        highest_airport = airport
    if h < lowest:
        lowest = h
        lowest_airport = airport

lowest_meters = ''
if lowest < 0:
    lowest_meters += '&#x2212;'
lowest_meters += str(int(abs(lowest)))


ages = [[flight.type2, round((flight.date - flight.first_flight).total_seconds() / 31557600.0, 2)]
        for flight in flights_objects if flight.first_flight is not None]
engines = [flight.engines for flight in flights_objects if flight.engines is not None]

distance_line()
distance_gram(segment_distances)
age_gram(ages)
engines_tally = engine_bar(engines)
time_gram()
delay_gram()

#make_map('mill')
#make_map('aeqd')
#make_map('na')
#make_map('eu')
#heat_map('na')

cities_inv = dict(cities)
cities = dict((v, k) for k in cities_inv for v in cities_inv[k])

cities_tally = collections.Counter()

for row in airports_tally:
    try:
        cities_tally[cities[row[0]]] += row[1]
    except KeyError:
        pass


def make_html():
    doc, tag, text = Doc().tagtext()

    with tag('html'):
        with tag('head'):
            doc.stag('meta', charset='utf-8')
            doc.stag('meta', name='robots', content='noindex,nofollow,noimageindex')
            doc.stag('link', rel='stylesheet', type='text/css', href='style.css')
            with tag('script', src='toggle.js'):
                text('')
            with tag('script', src='jquery-3.1.1.slim.min.js'):
                text('')
            with tag('title'):
                text('flights')

        with tag('body'):
            with tag('p', klass='code_link'):
                text('code available at ')
                with tag('a', target='_blank', href='https://bitbucket.org/rcc2139/'):
                    text('https://bitbucket.org/rcc2139/')

            with tag('div', klass='titles', onclick='toggle("airplanes","airplanesud");clean(document.body)'):
                # TODO: should be asis for "? but it works?
                with tag('span', id='airplanesud', klass='downarrow'):
                    text()
                text('airplanes')
            doc.stag('hr')

            with tag('div', id='airplanes', klass='tab_closed'):
                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('airplanes')
                    for row in airplanes_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(row[0])
                            with tag('td'):
                                with tag('a', klass='expand'):
                                    text(row[1])
                        with tag('tr', klass='toggle'):
                            with tag('td', colspan=2):
                                with tag('div'):
                                    with tag('div'):
                                        with tag('p', klass='pictures_row'):
                                            for pic in [element for list_element in
                                                        [flight.pics for flight in flights_objects if
                                                         flight.type2 == str(row[0]) and flight.pics is not None] for
                                                        element in
                                                        list_element]:  # flattens list
                                                with tag('a', target='_blank', href=pic):
                                                    doc.stag('img', src=pic, klass='log')
                                    with tag('div'):
                                        with tag('table', klass='abare'):
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('total distance')
                                                with tag('td'):
                                                    text(str(int(np.mean([x[1] for x in airplane_distances
                                                                          if x[0] == str(row[0])])) * row[1]) + ' km')
                                            if row[1] > 1:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('longest flight')
                                                    with tag('td'):
                                                        text(str(int(max([x for x in airplane_distances
                                                                          if x[0] == str(row[0])])[1])) + ' km')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('average flight')
                                                    with tag('td'):
                                                        text(str(int(np.mean([x[1] for x in airplane_distances
                                                                              if x[0] == str(row[0])]))) + ' km')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('shortest flight')
                                                    with tag('td'):
                                                        text(str(int(min([x for x in airplane_distances
                                                                          if x[0] == str(row[0])])[1])) + ' km')
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('first flown')
                                                with tag('td'):
                                                    text(str(min([x for x in airplane_dates
                                                                  if x[0] == str(row[0])])[1]))
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('last flown')
                                                with tag('td'):
                                                    text(str(max([x for x in airplane_dates
                                                                  if x[0] == str(row[0])])[1]))
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('variants')
                                                with tag('td'):
                                                    doc.asis(print_set(set([flight.type3 for flight in flights_objects
                                                                            if flight.type2 == str(row[0])])))
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('engines')
                                                with tag('td'):
                                                    doc.asis(print_set(set([flight.engines for flight in flights_objects
                                                                            if flight.type2 == str(row[0])])))
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('airlines')
                                                with tag('td'):
                                                    doc.asis(print_set(set([flight.mkt_cxr for flight in flights_objects
                                                                            if flight.type2 == str(row[0])])))

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('airlines')

                    for row in airlines_tally:
                        if row[0] in [x[0] for x in operated_by]:
                            expand_script = '''$(this).nextAll(':lt(''' + \
                                            str(len(operated_by[[x[0] for x in operated_by].index(row[0])][1:])) + \
                                            ''')').toggle()'''
                            with tag('tr', onClick=expand_script):
                                with tag('td'):
                                    doc.asis(row[0] + ' &#9654;')
                                with tag('td'):
                                    text(row[1])
                            for op_row in operated_by[[x[0] for x in operated_by].index(row[0])][1:]:
                                with tag('tr', style='display:none;'):
                                    with tag('td'):
                                        with tag('span', style='color:gray;font-style:italic;'):
                                            doc.asis('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
                                            text(op_row[0])
                                    with tag('td', style='text-align:right;'):
                                        with tag('span', style='color:gray;font-style:italic;'):
                                            text(op_row[1])
                        else:
                            with tag('tr'):
                                with tag('td'):
                                    text(row[0])
                                with tag('td'):
                                    text(row[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('manufacturers')
                    for row in manufacts_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(row[0])
                            with tag('td'):
                                text(row[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('engine manufacturers')
                    for row in engines_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(engine_manufacts_abbr[row[0]])
                            with tag('td'):
                                text(row[1])

            with tag('div', klass='titles', onclick='toggle("airports","airportsud")'):
                with tag('span', id='airportsud', klass='downarrow'):
                    text()
                text('locations')
            doc.stag('hr')

            with tag('div', id='airports', klass='tab_closed'):
                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('airports')
                    for row in airports_tally:
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(write_airport(Airport(row[0])))
                            with tag('td'):
                                text(row[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('cities')
                    for city, count in sorted(cities_tally.items(), key=lambda x: -x[1]):
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(write_city(city, airports_tally))
                            with tag('td'):
                                text(count)

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('American states')
                    for row in states_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(row[0])
                            with tag('td'):
                                text(row[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('countries')
                    for row in countries_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(row[0])
                            with tag('td'):
                                text(row[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('continents')
                    for row in continents_tally:
                        with tag('tr'):
                            with tag('td'):
                                text(row[0])
                            with tag('td'):
                                text(row[1])

            with tag('div', klass='titles', onclick='toggle("routes","routesud")'):
                with tag('span', id='routesud', klass='downarrow'):
                    text()
                text('routes')
            doc.stag('hr')

            with tag('div', id='routes', klass='tab_closed'):
                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('longest routes')
                    for s in segment_distances_norepeat[:20]:
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(write_segment(s[0]))
                            with tag('td'):
                                text(int(round(s[1], 1)))

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('shortest routes')
                    for s in [x for x in segment_distances_norepeat[::-1] if x[1] != 0][:20]:
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(write_segment(s[0]))
                            with tag('td'):
                                text(round(s[1], 1))

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('frequent routes')
                    for u in segment_counts[:20]:
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(write_segment([Airport(u[0][0]), Airport(u[0][1])]))
                            with tag('td'):
                                text(u[1])

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('discontinuities')
                    for u in land_routes:
                        with tag('tr'):
                            with tag('td'):
                                doc.asis(u[0])
                            with tag('td'):
                                text(str(int(u[1])) + ' km')

            # TODO: figure out the clean thing. adjusting line spacing is probably better than running weird javascript
            # and the cleans script does not clean the log
            with tag('div', klass='titles', onclick='toggle("maps","mapsud");clean(document.body)'):
                with tag('span', id='mapsud', klass='downarrow'):
                    text()
                text('maps')
            doc.stag('hr')

            with tag('div', id='maps', klass='tab_closed'):
                with tag('div'):
                    with tag('a', href='map_mill.png', target='_blank'):
                        doc.stag('img', src='map_mill.png', klass='maps_thumbs')
                    with tag('a', href='map_aeqd.png', target='_blank'):
                        doc.stag('img', src='map_aeqd.png', klass='maps_thumbs')
                with tag('div'):
                    with tag('a', href='map_na.png', target='_blank'):
                        doc.stag('img', src='map_na.png', klass='maps_thumbs')
                    with tag('a', href='map_eu.png', target='_blank'):
                        doc.stag('img', src='map_eu.png', klass='maps_thumbs')
                with tag('div'):
                    with tag('a', href='density.png', target='_blank'):
                        doc.stag('img', src='density.png', klass='maps_thumbs')

            with tag('div', klass='titles', onclick='toggle("graphs","graphsud")'):
                with tag('span', id='graphsud', klass='downarrow'):
                    text()
                text('graphs')
            doc.stag('hr')

            with tag('div', id='graphs', klass='tab_closed'):
                doc.stag('img', src='distance_line.png', klass='graphs')
                doc.stag('img', src='distance_gram.png', klass='graphs')
                doc.stag('img', src='age_gram.png', klass='graphs')
                doc.stag('img', src='time_gram.png', klass='graphs')
                doc.stag('img', src='delay_gram.png', klass='graphs')

            with tag('div', klass='titles', onclick='toggle("misc","miscud")'):
                with tag('span', id='miscud', klass='downarrow'):
                    text()
                text('misc')
            doc.stag('hr')

            with tag('div', id='misc', klass='tab_closed'):
                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=2):
                            text('tallies')
                    with tag('tr'):
                        with tag('td'):
                            text('flights')
                        with tag('td'):
                            text(num_flights)
                    with tag('tr'):
                        with tag('td'):
                            text('unique airports')
                        with tag('td'):
                            text(num_airports)
                    with tag('tr'):
                        with tag('td'):
                            text('unique airplane types')
                        with tag('td'):
                            text(num_types)
                    with tag('tr'):
                        with tag('td'):
                            text('total distance')
                        with tag('td'):
                            text(format(round(total_distance, 0), ","))
                            text(' km')
                            doc.stag('br')
                            text(round(circums, 2))
                            doc.asis(' &#xd7; 2&#x3c0;R')
                            with tag('sub'):
                                doc.asis('&#x2295;')
                    with tag('tr'):
                        with tag('td'):
                            text('mean segment distance')
                        with tag('td'):
                            text(round(mean_distance, 0))

                with tag('table', klass='log-table'):
                    with tag('tr'):
                        with tag('th', colspan=3):
                            text('superlatives')
                    with tag('tr'):
                        with tag('td'):
                            text('longest segment')
                        with tag('td'):
                            doc.asis(write_segment(longest_segment))
                        with tag('td'):
                            text(int(round(longest_segment_d, 0)))
                            text(' km')
                    with tag('tr'):
                        with tag('td'):
                            text('shortest segment')
                        with tag('td'):
                            doc.asis(write_segment(shortest_segment))
                        with tag('td'):
                            text(round(shortest_segment_d, 1))
                            text(' km')
                    with tag('tr'):
                        with tag('td'):
                            text('northernmost airport')
                        with tag('td'):
                            doc.asis(write_airport(northernmost_airport))
                        with tag('td'):
                            text(round(northernmost, 2))
                            doc.asis('&#176;')
                    with tag('tr'):
                        with tag('td'):
                            text('southernmost airport')
                        with tag('td'):
                            doc.asis(write_airport(southernmost_airport))
                        with tag('td'):
                            doc.asis(south_degrees)
                    with tag('tr'):
                        with tag('td'):
                            text('highest airport')
                        with tag('td'):
                            doc.asis(write_airport(highest_airport))
                        with tag('td'):
                            text(int(highest))
                            doc.asis(' m')
                    with tag('tr'):
                        with tag('td'):
                            text('lowest airport')
                        with tag('td'):
                            doc.asis(write_airport(lowest_airport))
                        with tag('td'):
                            doc.asis(lowest_meters)
                            text(' m')

            with tag('div', klass='titles', onclick='toggle("log","logud")'):
                with tag('span', id='logud', klass='uparrow'):
                    text()
                text('log')
            doc.stag('hr')

            with tag('div', id='log', style='margin-bottom:18px;display:block;'):
                with tag('table', klass='log-table'):
                    with tag('tr'):
                        headings = ['date', 'number', 'route', 'airline', 'airplane', 'photographs', '...']
                        for heading in headings:
                            with tag('th'):
                                text(heading)
                    for flight in [f for f in flights_objects if f.date.year == privacy_year]:
                        with tag('td'):
                            text(str(flight.date.strftime('%Y') or ''))
                        with tag('td'):
                            text(str(flight.desig or ''))
                        with tag('td'):
                            text('')
                        with tag('td'):
                            if flight.mkt_cxr == flight.adm_cxr or flight.adm_cxr is None:
                                text(str(flight.mkt_cxr or ''))
                            elif flight.mkt_cxr != flight.adm_cxr and flight.adm_cxr is not None:
                                text(str(flight.mkt_cxr or '') + ' (operated by ' + str(flight.adm_cxr or '') + ')')
                        with tag('td'):
                            text(str(flight.manufacturer or '') + ' ' + str(flight.type3 or '') +
                                 ' (' + str(flight.registration or '') + ')')
                        if flight.pics is not None:
                            with tag('td'):
                                for pic in flight.pics:
                                    with tag('a', target='_blank', href=pic):
                                        doc.stag('img', src=pic, klass='log')
                        else:
                            with tag('td'):
                                text('')
                        with tag('td'):
                            with tag('a', klass='expand'):
                                doc.stag('img', style='height:13px;', src='details.svg')

                        with tag('tr', klass='toggle'):
                            with tag('td', colspan=7, style='text-align:right;'):
                                if flight.pics is not None:
                                    with tag('div', style='display:inline-block;'):
                                        for pic in flight.pics:
                                            with tag('a', href=pic, target='_blank'):
                                                doc.stag('img', src=pic, klass='zoom')
                                with tag('div', style='display:inline-block;vertical-align:top;'):
                                    with tag('table', klass='bare'):
                                        if flight.cabin is not None:
                                            if flight.seat is not None:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('seat')
                                                    with tag('td'):
                                                        text(flight.seat + ' (' + flight.seat_type + ', ' +
                                                             flight.cabin + ')')
                                            elif flight.cabin is not None:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('seat')
                                                    with tag('td'):
                                                        text(flight.cabin)
                                        elif flight.seat is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('seat')
                                                with tag('td'):
                                                    text(flight.seat)
                                        if flight.msn is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('msn')
                                                with tag('td'):
                                                    text(flight.msn)
                                        if flight.ln is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('ln')
                                                with tag('td'):
                                                    text(flight.ln)
                                        if flight.first_flight is not None:
                                            if len(flight.first_flight_known) == 8:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        text(round((flight.date - flight.first_flight).total_seconds() /
                                                                   31557600.0, 2))
                                                        text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y %B %d'))
                                            elif len(flight.first_flight_known) == 6:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        years = int(round((flight.date -
                                                                           flight.first_flight).total_seconds() /
                                                                          31557600.0, 0))
                                                        text(years)
                                                        if years == 1:
                                                            text(' year')
                                                        else:
                                                            text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y %B'))
                                            elif len(flight.first_flight_known) == 4:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        years = int(round((flight.date -
                                                                           flight.first_flight).total_seconds() /
                                                                          31557600.0, 0))
                                                        text(years)
                                                        if years == 1:
                                                            text(' year')
                                                        else:
                                                            text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y'))
                                        if flight.engines is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('engines')
                                                with tag('td'):
                                                    doc.asis(flight.num_engines + ' &times; ' + flight.engines)


                    #for flight in flights_objects:
                    for flight in [f for f in flights_objects if f.date.year != privacy_year]:
                        with tag('td'):
                            text(str(flight.date.strftime('%Y %B %d') or ''))
                        with tag('td'):
                            text(str(flight.desig or '') + str(flight.number or ''))
                        with tag('td'):
                            doc.asis(str(flight.route_display))
                        with tag('td'):
                            if flight.mkt_cxr == flight.adm_cxr or flight.adm_cxr is None:
                                text(str(flight.mkt_cxr or ''))
                            elif flight.mkt_cxr != flight.adm_cxr and flight.adm_cxr is not None:
                                text(str(flight.mkt_cxr or '') + ' (operated by ' + str(flight.adm_cxr or '') + ')')

                        with tag('td'):
                            text(str(flight.manufacturer or '') + ' ' + str(flight.type3 or '') +
                                 ' (' + str(flight.registration or '') + ')')

                        if flight.pics is not None:
                            with tag('td'):
                                for pic in flight.pics:
                                    with tag('a', target='_blank', href=pic):
                                        doc.stag('img', src=pic, klass='log')
                        else:
                            with tag('td'):
                                text('')

                        with tag('td'):
                            with tag('a', klass='expand'):
                                doc.stag('img', style='height:13px;', src='details.svg')

                        with tag('tr', klass='toggle'):
                            with tag('td', colspan=7, style='text-align:right;'):
                                if flight.pics is not None:
                                    with tag('div', style='display:inline-block;'):
                                        for pic in flight.pics:
                                            with tag('a', href=pic, target='_blank'):
                                                doc.stag('img', src=pic, klass='zoom')
                                with tag('div', style='display:inline-block;vertical-align:top;'):
                                    with tag('table', klass='bare'):
                                        with tag('tr'):
                                            with tag('td'):
                                                text('gc distance')
                                            with tag('td'):
                                                text(int(flight.dist))
                                                text(' km')
                                        if flight.actual_dist is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('actual distance')
                                                with tag('td'):
                                                    text(int(flight.actual_dist))
                                                    text(' km')
                                        if flight.cabin is not None:
                                            if flight.seat is not None:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('seat')
                                                    with tag('td'):
                                                        text(flight.seat + ' (' + flight.seat_type + ', ' +
                                                             flight.cabin + ')')
                                            elif flight.cabin is not None:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('seat')
                                                    with tag('td'):
                                                        text(flight.cabin)
                                        elif flight.seat is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('seat')
                                                with tag('td'):
                                                    text(flight.seat)
                                        if flight.msn is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('msn')
                                                with tag('td'):
                                                    text(flight.msn)
                                        if flight.ln is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('ln')
                                                with tag('td'):
                                                    text(flight.ln)
                                        if flight.first_flight is not None:
                                            if len(flight.first_flight_known) == 8:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        text(round((flight.date - flight.first_flight).total_seconds() /
                                                                   31557600.0, 2))
                                                        text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y %B %d'))
                                            elif len(flight.first_flight_known) == 6:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        years = int(round((flight.date -
                                                                           flight.first_flight).total_seconds() /
                                                                          31557600.0, 0))
                                                        text(years)
                                                        if years == 1:
                                                            text(' year')
                                                        else:
                                                            text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y %B'))
                                            elif len(flight.first_flight_known) == 4:
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('age')
                                                    with tag('td'):
                                                        years = int(round((flight.date -
                                                                           flight.first_flight).total_seconds() /
                                                                          31557600.0, 0))
                                                        text(years)
                                                        if years == 1:
                                                            text(' year')
                                                        else:
                                                            text(' years')
                                                with tag('tr'):
                                                    with tag('td'):
                                                        text('first flight')
                                                    with tag('td'):
                                                        text(flight.first_flight.strftime('%Y'))
                                        if flight.engines is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('engines')
                                                with tag('td'):
                                                    doc.asis(flight.num_engines + ' &times; ' + flight.engines)
                                        if flight.runways is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('runways')
                                                with tag('td'):
                                                    for runway in flight.runways[:-1]:
                                                        text(runway + '/')
                                                    text(flight.runways[-1])
                                        if flight.gates is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('gates')
                                                with tag('td'):
                                                    for gate in flight.gates[:-1]:
                                                        text(gate + '/')
                                                    text(flight.gates[-1])
                                        if flight.std is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('std/sta')
                                                with tag('td'):
                                                    text(flight.std + '/' + (flight.sta or ''))
                                        if flight.atd is not None:
                                            with tag('tr'):
                                                with tag('td'):
                                                    text('atd/ata')
                                                with tag('td'):
                                                    text(flight.atd + '/' + (flight.ata or ''))

            with tag('script', id='jsbin-javascript'):
                text('$(".log-table td a.expand").click(function(){$(this).closest("tr").next().toggle();});')
            with tag('script'):
                doc.asis('function clean(node){for(var n = 0; n < node.childNodes.length; n ++){var child = node.childNodes[n];if(child.nodeType === 8 || (child.nodeType === 3 && !/\S/.test(child.nodeValue))){node.removeChild(child);n --;}else if(child.nodeType === 1){clean(child);}}}')
            #with tag('p'):
            #    with tag('a', href='#', onClick='clean(document.body)'):
            #        text('clean')
    return doc

doc = make_html()
result = indent(doc.getvalue())
f2 = open('flightsy.html', mode='w')
f2.write(result)
f2.close()
